package curso.umg.gt.umgapplogin;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class EstudianteActivity extends AppCompatActivity {

    private EditText edt1;
    private ListView lv1;
    private List<String> listado;
    private ArrayAdapter<String> adapter;
    private Spinner spinner;
    private String valor1;
    private String valor2;
    private RadioButton radioButton1;
    private RadioButton radioButton2;
    private RadioGroup radioGroup;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_estudiante);
        listado = new ArrayList<>();

        edt1= (EditText) findViewById(R.id.et1);
        lv1= (ListView) findViewById(R.id.lv1 );
        spinner = (Spinner) findViewById(R.id.spin);
        radioButton1 = (RadioButton) findViewById(R.id.radio_si);
        radioButton2 = (RadioButton) findViewById(R.id.radio_no);
        radioGroup = (RadioGroup) findViewById(R.id.opciones1);
        adapter=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,listado);
        lv1.setAdapter(adapter);

        final String[] valores = {"Ninguno","Masculino","Femenino"};
        spinner.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, valores));
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                Toast.makeText(adapterView.getContext(), (String) adapterView.getItemAtPosition(position), Toast.LENGTH_SHORT).show();
                valor1 = valores[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // vacio
            }
        });


    }
    public void add(View view){
        if (radioGroup.getCheckedRadioButtonId() == R.id.radio_si) {
            valor2 = "Si";
            Toast.makeText(this, valor2, Toast.LENGTH_LONG).show();
        } else if (radioGroup.getCheckedRadioButtonId() == R.id.radio_no) {
            valor2 = "No";
            Toast.makeText(this, valor2, Toast.LENGTH_LONG).show();
        }
        String value = edt1.getText().toString();
        listado.add("Edad: "+ value + ", Genero: " + valor1 + ", ¿Le Gusta el Futbol?: " + valor2);
        adapter.notifyDataSetChanged();
        edt1.setText("");
    }
}
